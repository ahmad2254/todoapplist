package ir.tavira.clinic.data.entity;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

import ir.tavira.clinic.utils.EncodeDecodeAES;

@Entity(nameInDb = "Work")
public class Work {
    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "title")
    private String title;
    @Property(nameInDb = "startdate")
    private long startdate = 0;
    @Property(nameInDb = "dateshow")
    private String dateshow = "0";
    @Property(nameInDb = "enddate")
    private long enddate = 0;
    @Property(nameInDb = "offset")
    private int offset;
    @Property(nameInDb = "type")
    private String type;
    @Property(nameInDb = "state")
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Work() {
    }

    public Work(String s) {
        title = s;
    }

    public Work(String s, int i, String today) {
        title = s;
        offset = i;
        type = today;
    }

    public String getDateshow() {
        return dateshow;
    }

    public void setDateshow(String dateshow) {
        this.dateshow = dateshow;
    }

    @Generated(hash = 742483426)
    public Work(Long id, String title, long startdate, String dateshow,
                long enddate, int offset, String type, String state) {
        this.id = id;
        this.title = title;
        this.startdate = startdate;
        this.dateshow = dateshow;
        this.enddate = enddate;
        this.offset = offset;
        this.type = type;
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getStartdate() {
        return startdate;
    }

    public void setStartdate(long startdate) {
        this.startdate = startdate;
    }

    public long getEnddate() {
        return enddate;
    }

    public void setEnddate(long enddate) {
        this.enddate = enddate;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void encriptData(EncodeDecodeAES aes) {
        try {
            String enctitle = EncodeDecodeAES.bytesToHex(aes.encrypt(title.trim()));
            title = enctitle;
//            startdate = Long.parseLong(EncodeDecodeAES.bytesToHex(aes.encrypt(startdate+"".trim())));
//            dateshow = EncodeDecodeAES.bytesToHex(aes.encrypt(dateshow.trim()));
//            enddate = Long.parseLong(EncodeDecodeAES.bytesToHex(aes.encrypt(enddate+"".trim())));
//            type = EncodeDecodeAES.bytesToHex(aes.encrypt(type.trim()));
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    public Work decriptData(EncodeDecodeAES aes) {
        try {
            String dectitle = new String(aes.decrypt(title));
            title = dectitle;
//            startdate = Long.parseLong(new String(aes.decrypt(startdate + "")));
//            dateshow = new String(aes.decrypt(dateshow));
//            enddate = Long.parseLong(new String(aes.decrypt(enddate + "")));
//            type = new String(aes.decrypt(type));
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return this;
    }
}
