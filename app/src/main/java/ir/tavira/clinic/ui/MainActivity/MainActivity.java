package ir.tavira.clinic.ui.MainActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import ir.tavira.clinic.R;
import ir.tavira.clinic.ui.Intro.IntroActivity;
import ir.tavira.clinic.ui.MainActivity.FragmentNarrator.FragmentNarrator;
import ir.tavira.clinic.ui.base.BaseActivity;

public class MainActivity extends BaseActivity implements MainMvpView, BottomNavigationView.OnNavigationItemSelectedListener {
    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Inject
    MainPresenter<MainMvpView> mPresenter;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;

    public static FragmentNarrator narrator;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("WrongConstant")
    @Override
    protected void setUp()
    {
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        if (mPresenter.getDataManager().getCurrentUserLoggedInMode() == 0) {
            startActivity(IntroActivity.getStartIntent(this));
            finish();
        } else {
            narrator = FragmentNarrator.getInstance(this);
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        narrator.swichPage(menuItem);
        return false;
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}