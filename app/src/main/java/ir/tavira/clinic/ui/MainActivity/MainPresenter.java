
package ir.tavira.clinic.ui.MainActivity;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import ir.tavira.clinic.data.DataManager;
import ir.tavira.clinic.ui.MainActivity.FragmentNarrator.FragmentNarrator;
import ir.tavira.clinic.ui.base.BasePresenter;
import ir.tavira.clinic.utils.rx.SchedulerProvider;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements
        MainMvpPresenter<V> {

    private static final String TAG = "MainPresenter";

    @Inject
    public MainPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
}
