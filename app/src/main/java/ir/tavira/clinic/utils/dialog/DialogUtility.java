package ir.tavira.clinic.utils.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.time.RadialPickerLayout;
import com.mohamadamin.persianmaterialdatetimepicker.time.TimePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import ir.tavira.clinic.data.entity.DateTime;

public class DialogUtility implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    public interface Listener {
        void showStartTime(DateTime selectedtime);
        void showEndTime(DateTime selectedtime);
    }
    private static final String TIMEPICKER = "TIMEPICKER";
    private Activity mcontext;
    private TimeWorkType mtype;
    private DateTime selectedtime;
    Listener mlistener;

    public DialogUtility(Activity mcontext, Listener listener) {
        this.mcontext = mcontext;
        selectedtime = new DateTime();
        mlistener = listener;
    }

    public void showSelectDate(TimeWorkType type) {
        mtype = type;
        PersianCalendar persianCalendar = new PersianCalendar();
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                this,
                persianCalendar.getPersianYear(),
                persianCalendar.getPersianMonth(),
                persianCalendar.getPersianDay()
        );
        datePickerDialog.show(mcontext.getFragmentManager(), "Datepickerdialog");
    }

    private void showTimePicker() {
        String fontName = "DroidNaskh-Regular";
        PersianCalendar now2 = new PersianCalendar();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                now2.get(PersianCalendar.HOUR_OF_DAY),
                now2.get(PersianCalendar.MINUTE),
                true
        );
        tpd.setThemeDark(false);
        tpd.setTypeface(fontName);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d(TIMEPICKER, "Dialog was cancelled");
            }
        });
        tpd.show(mcontext.getFragmentManager(), TIMEPICKER);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        showTimePicker();
        selectedtime.setYear(year);
        selectedtime.setMonth(monthOfYear + 1);
        selectedtime.setMday(dayOfMonth);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        selectedtime.setMhour(hourOfDay);
        selectedtime.setMminute(minute);
        showresult();
    }

    void showresult() {
        switch (mtype) {
            case STARTDATE:
                mlistener.showStartTime(selectedtime);
                break;
            case ENDDATE:
                mlistener.showEndTime(selectedtime);
                break;
        }
    }
}
