package ir.tavira.clinic.utils.date;

import ir.tavira.clinic.data.entity.DateTime;

public class DateValidateChecker {
    public static boolean checkdatevalidate(DateTime startdate, DateTime enddate) {

        if (startdate.getYear() < enddate.getYear()) {
            if (startdate.getYear() == enddate.getYear())// They are in one year
            {
                if (startdate.getMonth() < enddate.getMonth())
                    return true;
                else if (startdate.getMonth() > enddate.getMonth())//in valid date
                    return false;
                else if (startdate.getMonth() == enddate.getMonth()) // They are in one month
                    return true;
                else if (startdate.getMday() <= enddate.getMday())//in valid date
                    return true;
                else
                    return false;
            }

        }
        return true;
    }
}
