
package ir.tavira.clinic.ui.Intro;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import ir.tavira.clinic.data.DataManager;
import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.ui.MainActivity.MainActivity;
import ir.tavira.clinic.ui.base.BasePresenter;
import ir.tavira.clinic.utils.rx.SchedulerProvider;

public class IntroPresenter<V extends IntroMvpView> extends BasePresenter<V> implements
        IntroMvpPresenter<V> {

    private static final String TAG = "IntroPresenter";

    @Inject
    public IntroPresenter(DataManager dataManager,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
    @Override
    public void adddef_category(String name) {
        try {
            if (!name.equals("")) {
                getCompositeDisposable().add(getDataManager().insertCategory(new Category("پیش فرض"))
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(new Consumer<Boolean>() {
                            @Override
                            public void accept(Boolean aBoolean) throws Exception {
                                if (aBoolean) {
                                    getDataManager().setCurrentUserLoggedInMode(DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_IN);
                                    getDataManager().setCurrentUserName(name);
                                    getMvpView().login();
                                } else
                                    getMvpView().showMessage("");
                            }
                        })
                );
            } else {
                getMvpView().showMessage("لطفا نام خود را وارد کنید.");
            }
        } catch (Exception e) {

        }
    }
}
