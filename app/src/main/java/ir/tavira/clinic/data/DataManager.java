/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.data;


import ir.tavira.clinic.data.db.DbHelper;
import ir.tavira.clinic.data.network.ApiHelper;
import ir.tavira.clinic.data.prefs.PreferencesHelper;

/**
 * Created by janisharali on 27/01/17.
 */

public interface DataManager extends PreferencesHelper, ApiHelper, DbHelper
{
    enum LoggedInMode
    {
        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_LOGGED_IN(1);
        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }
}
