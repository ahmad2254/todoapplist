package ir.tavira.clinic.data.db;
import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.data.entity.DaoMaster;
import ir.tavira.clinic.data.entity.DaoSession;
import ir.tavira.clinic.data.entity.Work;
import ir.tavira.clinic.utils.EncodeDecodeAES;


@Singleton
public class AppDbHelper implements DbHelper {
    DaoSession daoSession;
    EncodeDecodeAES decodeAES=new EncodeDecodeAES();
    @Inject
    public AppDbHelper(DbOpenHelper helper) {
        Database db = helper.getEncryptedWritableDb("ahmad2254");
        daoSession = new DaoMaster(db).newSession();
    }

    @Override
    public Observable<Boolean> insertWork(Work newwork) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                newwork.encriptData(decodeAES);
                daoSession.getWorkDao().insert(newwork);
               return true;
            }
        });
    }

    @Override
    public Observable<Boolean> UpdateWork(Work newwork) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                daoSession.getWorkDao().update(newwork);
                return true;
            }
        });
    }

    @Override
    public Observable<List<Work>> getAllWorks() {
        return Observable.fromCallable(new Callable<List<Work>>() {
            @Override
            public List<Work> call() throws Exception {
                List<Work> list=daoSession.getWorkDao().loadAll();
                List<Work> temp=new ArrayList<>();
                for (Work work:list)
                   {
                       temp.add(work.decriptData(decodeAES)) ;
                   }
                return temp;
            }
        });
    }

    @Override
    public Observable<Boolean> insertCategory(Category newwork) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                daoSession.getCategoryDao().insert(newwork);
                return true;
            }
        });
    }

    @Override
    public Observable<List<Category>> getAllCategorys() {
        return Observable.fromCallable(new Callable<List<Category>>() {
            @Override
            public List<Category> call() throws Exception {
                return daoSession.getCategoryDao().loadAll();
            }
        });
    }
}
