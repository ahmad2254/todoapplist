package ir.tavira.clinic.ui.MainActivity;

import ir.tavira.clinic.di.scope.PerActivity;
import ir.tavira.clinic.ui.base.MvpPresenter;
import ir.tavira.clinic.ui.base.MvpView;

@PerActivity
public interface MainMvpPresenter <V extends MvpView> extends MvpPresenter<V> {
}
