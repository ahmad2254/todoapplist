package ir.tavira.clinic.ui.MainActivity.HomeFragment;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ir.tavira.clinic.R;
import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.data.entity.Work;
import ir.tavira.clinic.utils.AppConstants;

final class LoadContactsUseCase {
    ////////////////////// This class is made for data grouping by category
    Map<String, List<Work>> execute(@NonNull final Context context, List<Work> workList, List<Category> categorylist) {
        doinglist.clear();
        final Map<String, List<Work>> map = new LinkedHashMap<>();
        // String[] array=context.getResources().getStringArray(R.array.names);
        for (Category item : categorylist) {
            final List<Work> filteredContacts = getContactsWithLetter(
                    workList,
                    item.getTitle()
            );
            if (filteredContacts.size() > 0) {
                map.put(item.getTitle(), filteredContacts);
            }
        }
        if (doinglist.size() > 0) {
            map.put(AppConstants.TASK_STATE_DONE_PER_TITLE, doinglist);
        }
        return map;
    }

    //this list Keeps all finished work
    final List<Work> doinglist = new ArrayList<>();

    private List<Work> getContactsWithLetter(@NonNull final List<Work> names, final String letter) {
        final List<Work> contactsList = new ArrayList<>();
        for (final Work name : names) {
            if (name.getState().equals(AppConstants.TASK_STATE_DONE))
                doinglist.add(name);
            else if (name.getType().equals(letter)) {
                contactsList.add(name);
            }
        }
        return contactsList;
    }

}