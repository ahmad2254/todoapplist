package ir.tavira.clinic;
import android.content.Context;
import android.content.res.Configuration;
import javax.inject.Inject;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import ir.tavira.clinic.data.DataManager;

import ir.tavira.clinic.di.component.ApplicationComponent;
import ir.tavira.clinic.di.component.DaggerApplicationComponent;
import ir.tavira.clinic.di.module.ApplicationModule;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends MultiDexApplication {
    @Inject
    DataManager mDataManager;
    @Inject
    CalligraphyConfig mCalligraphyConfig;
    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
        mApplicationComponent.inject(this);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    public static App getApp(Context c) {
        return (App) c.getApplicationContext();
    }
    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    // Needed to replace the component with a drawer specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
