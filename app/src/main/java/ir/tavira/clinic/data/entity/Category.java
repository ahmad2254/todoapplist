package ir.tavira.clinic.data.entity;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity(nameInDb = "category")
public class Category {
    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "title")
    private String title;

    public Category() {
    }

    public Category(String s) {
        title = s;
    }

    @Generated(hash = 206903207)
    public Category(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
