package ir.tavira.clinic.ui.MainActivity.WorksFragment;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;
import ir.tavira.clinic.data.DataManager;
import ir.tavira.clinic.ui.base.BasePresenter;
import ir.tavira.clinic.utils.rx.SchedulerProvider;


public class WorkFragmentPresenter<V extends WorkFragmentMvpView> extends BasePresenter<V>
        implements WorkMvpPresenter<V> {


    @Inject
    public WorkFragmentPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
}
