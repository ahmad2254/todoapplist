package ir.tavira.clinic.ui.addNewWork;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.tavira.clinic.R;
import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.data.entity.DateTime;
import ir.tavira.clinic.data.entity.Work;
import ir.tavira.clinic.ui.base.BaseActivity;
import ir.tavira.clinic.utils.AppConstants;
import ir.tavira.clinic.utils.dialog.DialogUtility;
import ir.tavira.clinic.utils.dialog.TimeWorkType;

/// add new work page
//this page was created to add new work to local db
public class AddNewWorkActivity extends BaseActivity implements AddNewWorkMvpView, DialogUtility.Listener {


    //this inteface for refresh list data in HomeFragment page
    public interface listener {
        void viewrefresh();
    }

    /////this static method was create to get intent for lunch this page
    public static Intent getStartIntent(Context context, listener listener) {
        Intent intent = new Intent(context, AddNewWorkActivity.class);
        mlistener = listener;
        return intent;
    }

    Work mwork;
    private static listener mlistener;
    @BindView(R.id.topAppBar)
    MaterialToolbar topAppBar;

    @BindView(R.id.edt_startdate)
    TextInputEditText edt_startdate;

    @BindView(R.id.edt_enddate)
    TextInputEditText edt_enddate;
    @BindView(R.id.edt_title)
    TextInputEditText edt_title;
    @BindView(R.id.img_add_new_category)
    ImageView img_add_new_category;

    @BindView(R.id.spinner)
    Spinner spinner;

    @Inject
    AddNewWorkPresenter<AddNewWorkMvpView> mPresenter;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnewwork);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    DialogUtility dialogUtility;

    @SuppressLint("WrongConstant")
    @Override
    protected void setUp() {
        mwork = new Work();
        dialogUtility = new DialogUtility(this, this);
        topAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        topAppBar.setOnMenuItemClickListener(this::onMenuItemClick);
        edt_enddate.setOnClickListener(this::onClick);
        edt_startdate.setOnClickListener(this::onClick);
        img_add_new_category.setOnClickListener(this::onClick);
        // get the category from db
        mPresenter.getCategory();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_add_new_category:
                showDialog();
                break;
            case R.id.edt_startdate:
                dialogUtility.showSelectDate(TimeWorkType.STARTDATE);
                break;
            case R.id.edt_enddate:
                dialogUtility.showSelectDate(TimeWorkType.ENDDATE);
                break;
        }

    }

    public boolean onMenuItemClick(MenuItem item) {
        try {
            mwork.setTitle(edt_title.getText().toString());
            mwork.setType(spinner.getSelectedItem().toString());
            mwork.setState(AppConstants.TASK_STATE_WAIT);
            mPresenter.addWorkTodb(mwork);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void showDialog() {
        final View customLayout = getLayoutInflater().inflate(R.layout.add_new_category, null);
        TextInputEditText input = customLayout.findViewById(R.id.edt_titlee);
        MaterialAlertDialogBuilder dialog = new MaterialAlertDialogBuilder(this)
                .setView(customLayout)
                .setPositiveButton(R.string.str_cancell, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).setNegativeButton(R.string.str_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        mPresenter.addnew_category(new Category(input.getText().toString()));
                    }
                });
        dialog.show();
    }

    @Override
    public void addSucsses() {
        mlistener.viewrefresh();
        finish();
    }

    @Override
    public void addCategorySucsses() {
        mPresenter.getCategory();
    }

    @Override
    public void showCategory(String[] list) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }


    @Override
    public void showStartTime(DateTime selectedtime) {
        mwork.setStartdate(selectedtime.getunixformat());
        edt_startdate.setText(selectedtime.getpersianformat().toString());
        Log.e("chackdate", selectedtime.getmiladyformat());
    }

    @Override
    public void showEndTime(DateTime selectedtime) {
        if (mwork.getStartdate() < selectedtime.getunixformat()) {
            mwork.setEnddate(selectedtime.getunixformat());
            mwork.setDateshow(selectedtime.getpersianformat());
            edt_enddate.setText(selectedtime.getpersianformat().toString());
        } else
            showMessage("تاریخ نا معتبر");

    }
    //1593069545 start
    //1593155858  end

}
