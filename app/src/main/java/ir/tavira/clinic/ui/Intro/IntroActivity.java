package ir.tavira.clinic.ui.Intro;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.tavira.clinic.R;
import ir.tavira.clinic.data.DataManager;
import ir.tavira.clinic.ui.MainActivity.MainActivity;
import ir.tavira.clinic.ui.MainActivity.MainMvpView;
import ir.tavira.clinic.ui.MainActivity.MainPresenter;
import ir.tavira.clinic.ui.base.BaseActivity;

public class IntroActivity extends BaseActivity implements IntroMvpView {
    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, IntroActivity.class);
        return intent;
    }

    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.edt_name)
    EditText edt_name;
    @Inject
    IntroPresenter<IntroMvpView> mPresenter;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @SuppressLint("WrongConstant")
    @Override
    protected void setUp() {
        btn_login.setOnClickListener(this::onClick);
    }

    public void onClick(View view) {
        mPresenter.adddef_category(edt_name.getText().toString());
    }


    @Override
    public void login() {
            startActivity(MainActivity.getStartIntent(this));
            finish();
    }
}