package ir.tavira.clinic.ui.MainActivity.FragmentNarrator;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import ir.tavira.clinic.R;
import ir.tavira.clinic.ui.MainActivity.HomeFragment.HomeFragment;
import ir.tavira.clinic.ui.MainActivity.WorksFragment.WorkFragment;
//this list Keeps all fragments and manage all to showing
public class FragmentNarrator {
    public static HomeFragment homeFragment = HomeFragment.newInstance();
    public static WorkFragment workFragment = WorkFragment.newInstance();

    FragmentTransaction ft;
    private static AppCompatActivity mContext;
    private static FragmentNarrator instance;

    public static FragmentNarrator getInstance(AppCompatActivity context) {
        if (instance == null)
            instance = new FragmentNarrator();
        mContext = context;
        return instance;
    }

    private void setFragmentTransaction(FragmentTransaction fragmentTransaction) {
        this.ft = fragmentTransaction;
    }

    private void onShowHomeFragment() {
        try {
            if (homeFragment.isAdded()) { // if the fragment is already in container
                ft.show(homeFragment);
            } else { // fragment needs to be added to frame container
                ft.add(R.id.cl_root_view, homeFragment, HomeFragment.TAG);
            }
            if (workFragment.isAdded())
                ft.hide(workFragment);
            ft.commit();
        } catch (Exception e) {
        }
    }


    private void onShowWorksFragment() {
        try {
            if (workFragment.isAdded()) { // if the fragment is already in container
                ft.show(workFragment);
            } else { // fragment needs to be added to frame container
                ft.add(R.id.cl_root_view, workFragment, WorkFragment.TAG);
            }
            if (homeFragment.isAdded())
                ft.hide(homeFragment);
            ft.commit();
        } catch (Exception e) {
        }
    }

    public void swichPage(@NonNull MenuItem menuItem){
        setFragmentTransaction(mContext.getSupportFragmentManager().beginTransaction());
        if (menuItem.getItemId() == R.id.nav_home) {
            onShowHomeFragment();
            menuItem.setChecked(true);
        } else if (menuItem.getItemId() == R.id.nave_works) {
            onShowWorksFragment();
            menuItem.setChecked(true);
        }
    }
}
