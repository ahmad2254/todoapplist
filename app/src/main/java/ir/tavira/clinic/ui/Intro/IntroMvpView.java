
package ir.tavira.clinic.ui.Intro;
import ir.tavira.clinic.ui.base.MvpView;

public interface IntroMvpView extends MvpView
{
   void login();
}
