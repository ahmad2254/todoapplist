package ir.tavira.clinic.ui.addNewWork;

import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.data.entity.Work;
import ir.tavira.clinic.di.scope.PerActivity;
import ir.tavira.clinic.ui.base.MvpPresenter;
import ir.tavira.clinic.ui.base.MvpView;

@PerActivity
public interface AddNewWorkMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void onViewInitialized();
    void addWorkTodb(Work work) throws Exception;
    void addnew_category(Category category);
}
