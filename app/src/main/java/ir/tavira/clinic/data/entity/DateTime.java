package ir.tavira.clinic.data.entity;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ir.tavira.clinic.R;
import ir.tavira.clinic.utils.date.CivilDate;
import ir.tavira.clinic.utils.date.PersianDate;

import static ir.tavira.clinic.utils.date.DateConverter.civilToPersian;
import static ir.tavira.clinic.utils.date.DateConverter.persianToCivil;
import static ir.tavira.clinic.utils.date.DateConverter.persianToJdn;

public class DateTime {
    private int year = 0;
    private int month = 0;
    private int mday = 0;
    private int mhour = 0;
    private int mminute = 0;
    PersianCalendar persianCalendar;

    public DateTime() {
        persianCalendar = new PersianCalendar();
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getMday() {
        return mday;
    }

    public void setMday(int mday) {
        this.mday = mday;
    }

    public int getMhour() {
        return mhour;
    }

    public void setMhour(int mhour) {
        this.mhour = mhour;
    }

    public int getMminute() {
        return mminute;
    }

    public void setMminute(int mminute) {
        this.mminute = mminute;
    }

    public DateTime(int year, int month, int mday, int mhour, int mminute) {
        this.year = year;
        this.month = month;
        this.mday = mday;
        this.mhour = mhour;
        this.mminute = mminute;
    }

    public String getpersianformat() {
        return year + " / " + month + " / " + mday + "  " + mminute + " : " + mhour;
    }

    public String getmiladyformat() {
        persianCalendar.setPersianDate(year, month , mday-1);
        CivilDate c = new CivilDate(persianCalendar);
        Calendar c2=Calendar.getInstance();
        c2.set(c.getYear(),c.getMonth(),c.getDayOfMonth());
        String formattedDate =  "";
        return formattedDate;
    }

    public long getunixformat() {
        persianCalendar.setPersianDate(year, month - 1, mday);
        CivilDate c = new CivilDate(persianCalendar);
        Calendar c2=Calendar.getInstance();
        c2.set(c.getYear(),c.getMonth(),c.getDayOfMonth(),mhour,mminute);
        long unixtime= c2.getTime().getTime() / 1000L;;
        return  unixtime;
    }


}


