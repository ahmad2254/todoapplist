package ir.tavira.clinic.data.db;

import java.util.List;

import io.reactivex.Observable;
import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.data.entity.Work;

public interface DbHelper {
    Observable<Boolean> insertWork(Work newwork);
    Observable<Boolean> UpdateWork(Work newwork);
    Observable<List<Work>> getAllWorks();

    Observable<Boolean> insertCategory(Category newwork);
    Observable<List<Category>> getAllCategorys();
}
