
package ir.tavira.clinic.ui.addNewWork;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import ir.tavira.clinic.R;
import ir.tavira.clinic.data.DataManager;
import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.data.entity.Work;
import ir.tavira.clinic.ui.base.BasePresenter;
import ir.tavira.clinic.utils.EncodeDecodeAES;
import ir.tavira.clinic.utils.rx.SchedulerProvider;

public class AddNewWorkPresenter<V extends AddNewWorkMvpView> extends BasePresenter<V> implements
        AddNewWorkMvpPresenter<V> {

    private static final String TAG = "AddNewWorkPresenter";

    @Inject
    public AddNewWorkPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewInitialized() {
        getCategory();
    }

    @Override
    public void addWorkTodb(Work work) throws Exception {
        if (work != null) {
            if (work.getTitle().equals("")) {
                getMvpView().showMessage(R.string.str_error_title_empty);
                return;
            }
            if (work.getStartdate() == 0)
                getMvpView().showMessage(R.string.str_error_startdate);
            else  if (work.getEnddate() == 0)
                getMvpView().showMessage(R.string.str_error_enddate);
            else {
                getCompositeDisposable().add(getDataManager().insertWork(work)
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(new Consumer<Boolean>() {
                            @Override
                            public void accept(Boolean aBoolean) throws Exception {
                                if (aBoolean)
                                    getMvpView().addSucsses();
                                else
                                    getMvpView().showMessage("لطفا دوباره تلاش کنید");
                            }
                        })
                );
            }
        } else {

        }
    }

    @Override
    public void addnew_category(Category category) {
        if (category.getTitle().equals("")) {
            getMvpView().showMessage("اطلاعات را وارد کنید");
            return;
        } else
            getCompositeDisposable().add(getDataManager().insertCategory(category)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) throws Exception {
                            if (aBoolean) {
                                getMvpView().addCategorySucsses();
                            } else
                                getMvpView().showMessage("false");
                        }
                    })
            );
    }

    public void getCategory() {
        getCompositeDisposable().add(getDataManager().getAllCategorys()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Category>>() {
                    @Override
                    public void accept(List<Category> items) throws Exception {
                        if (items.size() > 0) {
                            String[] moviesList = new String[items.size()];
                            int i = 0;
                            for (Category item : items) {
                                moviesList[i] = item.getTitle();
                                i++;
                            }
                            getMvpView().showCategory(moviesList);
                        } else
                            getMvpView().showMessage(items.size() + "");
                    }
                })
        );
    }

}
