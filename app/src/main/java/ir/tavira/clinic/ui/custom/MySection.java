package ir.tavira.clinic.ui.custom;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.Section;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import ir.tavira.clinic.R;
import ir.tavira.clinic.data.entity.Work;
import ir.tavira.clinic.utils.AppConstants;
import ir.tavira.clinic.utils.CommonUtils;

public final class MySection extends Section {

    private final String title;
    private final List<Work> list;
    private final ClickListener clickListener;
    private final Context mcontext;

    public MySection(@NonNull final String title, @NonNull final List<Work> list,
                     @NonNull final ClickListener clickListener, @NonNull final Context context) {

        super(SectionParameters.builder()
                .itemResourceId(R.layout.section_ex1_item)
                .headerResourceId(R.layout.section_ex1_header)
                .build());

        this.title = title;
        this.list = list;
        this.clickListener = clickListener;
        this.mcontext = context;
    }

    @Override
    public int getContentItemsTotal() {
        return list.size();
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(final View view) {
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;
        final Work contact = list.get(position);
        itemHolder.tvItem.setText(contact.getTitle());
        itemHolder.txt_date.setText(contact.getDateshow());
        if (contact.getState().equals(AppConstants.TASK_STATE_DONE)) {
            itemHolder.img_confirm.setEnabled(false);
//               itemHolder.img_confirm.setImageDrawable(CommonUtils.changeIconcolor(mcontext,R.drawable.ic_baseline_check_black_24));
            itemHolder.img_confirm.setImageResource(R.drawable.ic_baseline_check_grey_24);
            itemHolder.txt_type.setVisibility(View.VISIBLE);
            itemHolder.txt_type.setText(contact.getType());

        }
        itemHolder.img_confirm.setOnClickListener(v ->
                clickListener.onItemClicked(contact)

        );
        itemHolder.rootView.setOnClickListener(v ->
                clickListener.onItemRootViewClicked(this, 0)
        );
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(final View view) {
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(final RecyclerView.ViewHolder holder) {
        final HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
        headerHolder.tvTitle.setText(title);

        //      headerHolder.bg.setBackgroundColor(Color.parseColor("#BA68C8"));
    }

    public interface ClickListener {

        void onItemRootViewClicked(@NonNull final MySection section, final int itemAdapterPosition);

        void onItemClicked(@NonNull final Work work);
    }
}

final class HeaderViewHolder extends RecyclerView.ViewHolder {

    final TextView tvTitle;

    HeaderViewHolder(@NonNull View view) {
        super(view);
        tvTitle = view.findViewById(R.id.tvTitle);

    }
}

final class ItemViewHolder extends RecyclerView.ViewHolder {
    final View rootView;
    final TextView tvItem, txt_date, txt_type;
    final ImageView img_confirm;

    ItemViewHolder(@NonNull View view) {
        super(view);
        rootView = view;
        tvItem = view.findViewById(R.id.tvItem);
        txt_date = view.findViewById(R.id.txt_date);
        txt_type = view.findViewById(R.id.txt_type);
        img_confirm = view.findViewById(R.id.img_confirm);
    }
}