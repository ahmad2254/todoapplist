/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.data.db;

import android.content.Context;
import android.util.Log;

import org.greenrobot.greendao.database.Database;

import javax.inject.Inject;
import javax.inject.Singleton;

import ir.tavira.clinic.data.entity.DaoMaster;
import ir.tavira.clinic.di.qualifier.ApplicationContext;
import ir.tavira.clinic.di.qualifier.DatabaseInfo;

//@Singleton
public class DbOpenHelper extends DaoMaster.DevOpenHelper {
  //  DaoMaster    daoSession;
    @Inject
    public DbOpenHelper(@ApplicationContext Context context, @DatabaseInfo String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        Log.e("testgreen", "DB_OLD_VERSION : " + oldVersion + ", DB_NEW_VERSION : " + newVersion);
//        AppLogger.d("DEBUG", "DB_OLD_VERSION : " + oldVersion + ", DB_NEW_VERSION : " + newVersion);
        switch (oldVersion) {
            case 1:
            case 2:
                //db.execSQL("ALTER TABLE " + UserDao.TABLENAME + " ADD COLUMN "
                // + UserDao.Properties.Name.columnName + " TEXT DEFAULT 'DEFAULT_VAL'");
        }

    }


//    public static void clearDatabase(Context context) {
//        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(
//                context.getApplicationContext(), DB_NAME, null);
//        SQLiteDatabase db = devOpenHelper.getWritableDatabase();
//        devOpenHelper.onUpgrade(db,0,0);
//    }


}
