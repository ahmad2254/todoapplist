package ir.tavira.clinic.ui.MainActivity.HomeFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import ir.tavira.clinic.data.DataManager;
import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.data.entity.Work;
import ir.tavira.clinic.ui.base.BasePresenter;
import ir.tavira.clinic.utils.rx.SchedulerProvider;


public class HomeFragmentPresenter<V extends HomeFragmentMvpView> extends BasePresenter<V>
        implements HomeMvpPresenter<V> {
    @Inject
    public HomeFragmentPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void init() {
//        List<Work> works = new ArrayList<>();
//        works.add(new Work(1,"job 1",10,"Today"));
//        works.add(new Work(2,"job 2",20,"Lastweek"));
//        works.add(new Work(3,"job 3",25,"Doing"));
//        works.add(new Work(4,"job 4",38,"Today"));
//        works.add(new Work(5,"job 5",30,"Lastweek"));
//        getMvpView().showWorks(works);
        getCategory();
    }

    @Override
    public void getWorks(List<Category> categoryList) {
        getCompositeDisposable().add(getDataManager().getAllWorks()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Work>>() {
                    @Override
                    public void accept(List<Work> items) throws Exception {
                        if (items.size() > 0) {
                            getMvpView().showWorks(items, categoryList);
                            getMvpView().emptylist(false);
                        } else {
                            getMvpView().emptylist(true);
                        }
                    }
                })
        );
    }

    @Override
    public void UpdateWorkState(Work work) {
        try {
            getCompositeDisposable().add(getDataManager().UpdateWork(work)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) throws Exception {
                            if (aBoolean) {
                                getMvpView().updateSuccess();
                            } else
                                getMvpView().showMessage("لطفا مجدد تلاش کنید.");
                        }
                    })
            );
        } catch (Exception e) {

        }
    }

    public void getCategory() {
        getCompositeDisposable().add(getDataManager().getAllCategorys()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<Category>>() {
                    @Override
                    public void accept(List<Category> items) throws Exception {
                        if (items.size() > 0)
                            getWorks(items);
                        else
                            getMvpView().showMessage(items.size() + "");
                    }
                })
        );
    }
}
