/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package ir.tavira.clinic.data;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import ir.tavira.clinic.data.db.DbHelper;
import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.data.entity.Work;
import ir.tavira.clinic.data.network.ApiHelper;
import ir.tavira.clinic.data.prefs.PreferencesHelper;
import ir.tavira.clinic.di.qualifier.ApplicationContext;


/**
 * Created by janisharali on 27/01/17.
 */
@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;
    private final DbHelper mdbHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context, PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper, DbHelper dbHelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
        mdbHelper = dbHelper;
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String name) {
        mPreferencesHelper.setCurrentUserName(name);
    }

    @Override
    public Observable<Boolean> insertWork(Work newwork) {
        return mdbHelper.insertWork(newwork);
    }

    @Override
    public Observable<Boolean> UpdateWork(Work newwork) {
        return mdbHelper.UpdateWork(newwork);
    }

    @Override
    public Observable<List<Work>> getAllWorks() {
        return mdbHelper.getAllWorks();
    }

    @Override
    public Observable<Boolean> insertCategory(Category newwork) {
      return   mdbHelper.insertCategory(newwork);
    }

    @Override
    public Observable<List<Category>> getAllCategorys() {
        return mdbHelper.getAllCategorys();
    }
}
