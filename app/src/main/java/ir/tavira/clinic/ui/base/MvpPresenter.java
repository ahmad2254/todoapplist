
package ir.tavira.clinic.ui.base;
public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);
    void onDetach();
    void handleError(String code);
}
