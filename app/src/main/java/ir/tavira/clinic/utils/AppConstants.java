
package ir.tavira.clinic.utils;
public final class AppConstants {
    public static final String TASK_STATE_DONE = "done";
    public static final String TASK_STATE_DOING = "doing";
    public static final String TASK_STATE_CANCEL = "cancel";
    public static final String TASK_STATE_WAIT = "wait";
    public static final String TASK_STATE_DONE_PER_TITLE = "کارهای انجام شده";
    public static final String PREF_NAME = "mindorks_pref";
    public static final String DB_NAME = "mybd.db";
    public static final long NULL_INDEX = -1L;
    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
