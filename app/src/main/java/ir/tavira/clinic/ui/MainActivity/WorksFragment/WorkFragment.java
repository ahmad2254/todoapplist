/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.ui.MainActivity.WorksFragment;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import javax.inject.Inject;
import butterknife.ButterKnife;
import ir.tavira.clinic.R;
import ir.tavira.clinic.di.component.ActivityComponent;
import ir.tavira.clinic.ui.base.BaseFragment;

public class WorkFragment extends BaseFragment implements
        WorkFragmentMvpView {
    public static final String TAG = "WorkFragment";

    @Inject
    WorkFragmentPresenter<WorkFragmentMvpView> mPresenter;

    public static WorkFragment newInstance() {
        Bundle args = new Bundle();
        WorkFragment fragment = new WorkFragment();
//        if (model != null) args.putParcelable("SampleObject", model);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_works, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
         return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void setUp(View view) {

    }
}

