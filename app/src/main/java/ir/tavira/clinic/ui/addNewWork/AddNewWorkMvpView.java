
package ir.tavira.clinic.ui.addNewWork;

import java.util.List;

import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.ui.base.MvpView;

public interface AddNewWorkMvpView extends MvpView {
    void addSucsses();
    void addCategorySucsses();
    void showCategory(String[] list);
}
