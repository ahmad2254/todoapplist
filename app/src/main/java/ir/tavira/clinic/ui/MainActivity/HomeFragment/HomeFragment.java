/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ir.tavira.clinic.ui.MainActivity.HomeFragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import ir.tavira.clinic.R;
import ir.tavira.clinic.data.entity.Category;
import ir.tavira.clinic.data.entity.Work;
import ir.tavira.clinic.di.component.ActivityComponent;
import ir.tavira.clinic.ui.addNewWork.AddNewWorkActivity;
import ir.tavira.clinic.ui.base.BaseFragment;
import ir.tavira.clinic.ui.custom.MySection;
import ir.tavira.clinic.utils.AppConstants;

public class HomeFragment extends BaseFragment implements
        HomeFragmentMvpView, MySection.ClickListener, AddNewWorkActivity.listener {
    public static final String TAG = "HomeFragment";
    @Inject
    HomeFragmentPresenter<HomeFragmentMvpView> mPresenter;
    @BindView(R.id.topAppBar)
    MaterialToolbar topAppBar;
    @Inject
    SectionedRecyclerViewAdapter sectionAdapter;
    @BindView(R.id.floatingActionButton)
    FloatingActionButton floatingbtn;
    @BindView(R.id.recycler_vertical)
    RecyclerView recycler_vertical;
    @BindView(R.id.empty_view)
    LinearLayout empty_view;
    @Inject
    LinearLayoutManager layoutManagervertical;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
//        if (model != null) args.putParcelable("SampleObject", model);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void setUp(View view) {
        topAppBar.setOnMenuItemClickListener(this::onMenuItemClick);
        floatingbtn.setOnClickListener(this::onClick);
        topAppBar.setTitle(mPresenter.getDataManager().getCurrentUserName());
        layoutManagervertical.setOrientation(RecyclerView.VERTICAL);
        recycler_vertical.setLayoutManager(layoutManagervertical);
        mPresenter.init();
    }

    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_setting:
                showDialog();
                break;
            case R.id.nav_exit:
                showExitDialog();
                break;
        }
        return false;
    }

    void onClick(View v) {
        startActivity(AddNewWorkActivity.getStartIntent(getContext(), this));
    }

    @Override
    public void showWorks(List<Work> works, List<Category> categoryList) {
        sectionAdapter.removeAllSections();
        final Map<String, List<Work>> contactsMap = new LoadContactsUseCase().execute(requireContext(), works, categoryList);
        for (final Map.Entry<String, List<Work>> entry : contactsMap.entrySet()) {
            if (entry.getValue().size() > 0) {
                sectionAdapter.addSection(new MySection(entry.getKey(), entry.getValue(), this, getContext()));
            }
        }
        recycler_vertical.setAdapter(sectionAdapter);
    }

    @Override
    public void updateSuccess() {
        viewrefresh();
    }

    @Override
    public void emptylist(boolean state) {
        if (state)
            empty_view.setVisibility(View.VISIBLE);
        else
            empty_view.setVisibility(View.GONE);
    }

    @Override
    public void onItemRootViewClicked(@NonNull MySection section, int itemAdapterPosition) {
        showMessage(itemAdapterPosition + "");
    }

    @Override
    public void onItemClicked(@NonNull Work work) {
        work.setState(AppConstants.TASK_STATE_DONE);
        mPresenter.UpdateWorkState(work);
    }

    @Override
    public void viewrefresh() {
        mPresenter.init();
    }

    private void showDialog() {
        final int[] checkedItem = {1};
        MaterialAlertDialogBuilder dialog = new MaterialAlertDialogBuilder(getContext())
                .setPositiveButton(R.string.str_cancell, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }).setNegativeButton(R.string.str_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        dialog.setSingleChoiceItems(getResources().getStringArray(R.array.filterlist), checkedItem[0], new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                checkedItem[0] = i;
            }
        });
        dialog.show();
    }

    private void showExitDialog() {
        MaterialAlertDialogBuilder dialog = new MaterialAlertDialogBuilder(getContext())
                .setTitle(getResources().getString(R.string.str_exit))
                .setMessage(R.string.str_ask_exit)
                .setPositiveButton(R.string.str_cancell, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }).setNegativeButton(R.string.str_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        Objects.requireNonNull(getActivity()).finish();
                    }
                });
        dialog.show();
    }
}

